<?php


class Conexion
{
    /**
     * Setear usuario y contrasena de la base de datos
     */

    /**
     * @return PDO
     */
    public static function conectarse(){
        $usuario='root';
        $contrasena='';

        try {
            $mbd = new PDO('mysql:host=localhost;dbname=test_soap',$usuario, $contrasena);
           return $mbd;
        } catch (PDOException $e) {
            print "¡Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}