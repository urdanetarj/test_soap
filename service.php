<?php
include_once ('lib/nusoap.php');
include_once ('Conexion.php');
$namespace = $_SERVER['SERVER_NAME']."/test_soap/service.php";
$server = new soap_server();
$server->configureWSDL("TestSOAP",$namespace);
$server->wsdl->schemaTargetNamespace = $namespace;

$server->wsdl->addComplexType(
'addUser',
'complexType',
'struct',
'all',
'',
array(
'username' => array('name' => 'username', 'type'=>'xsd:string'),
'password' => array('name' => 'password', 'type'=>'xsd:string'),
'email' => array('name' => 'email', 'type'=>'xsd:string'),
)
);

$server->wsdl->addComplexType(
'response',
'complexType',
'struct',
'all',
'',
array(
'Resultado' => array('name' => 'Resultado', 'type' => 'xsd:boolean')
)
);


$server->register(
'addUser',
array('name' => 'tns:addUser'),
array('name' => 'tns:response'),
$namespace,
"urn:".$namespace."/addUser",
'rpc',
'encoded',
'Recibe una username,password,email y devuelve una respuesta boolean =true o false'
);


$server->wsdl->addComplexType(
    'activateUser',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' => array('name' => 'username', 'type'=>'xsd:string'),
    )
);

$server->wsdl->addComplexType(
    'response',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'Resultado' => array('name' => 'Resultado', 'type' => 'xsd:boolean')
    )
);


$server->register(
    'activateUser',
    array('name' => 'tns:activateUser'),
    array('name' => 'tns:response'),
    $namespace,
    "urn:".$namespace."/activateUser",
    'rpc',
    'encoded',
    'Recibe una username y devuelve una respuesta boolean =true o false en caso de encontrarlo'
);



$server->wsdl->addComplexType(
    'desactivateUser',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' => array('name' => 'username', 'type'=>'xsd:string'),
    )
);

$server->wsdl->addComplexType(
    'response',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'Resultado' => array('name' => 'Resultado', 'type' => 'xsd:boolean')
    )
);


$server->register(
    'desactiveUser',
    array('name' => 'tns:desactivateUser'),
    array('name' => 'tns:response'),
    $namespace,
    "urn:".$namespace."/desactivateUser",
    'rpc',
    'encoded',
    'Recibe una username y devuelve una respuesta boolean =true o false en caso de encontrarlo'
);


$server->wsdl->addComplexType(
    'getUser',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' => array('name' => 'username', 'type'=>'xsd:string'),
    )
);

$server->wsdl->addComplexType(
    'response',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'Resultado' => array('name' => 'Resultado', 'type' => 'xsd:string'),
        array('name' => 'Resultado', 'type' => 'xsd:string'),
        array('name' => 'Resultado', 'type' => 'xsd:string')
    )

);


$server->register(
    'getUser',
    array('name' => 'tns:getUser'),
    array('name' => 'tns:response'),
    $namespace,
    "urn:".$namespace."/getUser",
    'rpc',
    'encoded',
    'Recibe una username y devuelve username,password , email'
);







function getUser($request){
    $conexion=  Conexion::conectarse();

    if(isset($request['username'])){
        $username=$request['username'];

        $sql="SELECT * FROM users WHERE username='$username'";
        $resultado=$conexion->prepare($sql);
        $resultado->execute();
        $res=$resultado->fetch();

        /**
         * Devolvemos el getUser
         */
        if(!empty($res)){

            error_log('Procesamos request getUser | username: '.$request['username']);
            return array(
                "Resultado" =>$res['username'],$res['password'],$res['email']
            );
        }else{
            error_log('Procesamos request getUser | username: '.$request['username']);
            return array(
                "Resultado" => false
            );
        }

    }
}

function desactivateUser($request){
    $conexion=  Conexion::conectarse();

    if(isset($request['username'])){
        $username=$request['username'];
        $sql="SELECT * FROM users WHERE username='$username' AND borrado=1";
        $resultado=$conexion->prepare($sql);
        $resultado->execute();
        $res=$resultado->fetch();

        /**
         * Si el usaurio esta desactivo devuelve status_code=0 sino false
         */
        if(!empty($res)){
            error_log('Procesamos request desactivateUser | username: '.$request['username']);
            return array(
                "Resultado" =>'status_code =0'
            );
        }else{
            error_log('Procesamos request desactivateUser | username: '.$request['username']);
            return array(
                "Resultado" => false
            );
        }

    }else{
        error_log('Hubo un error al intentar procesar request desactiveUser');
    }
}


function activateUser($request){
    $conexion=  Conexion::conectarse();

        if(isset($request['username'])){
            $username=$request['username'];
            $sql="SELECT * FROM users WHERE username='$username' AND borrado=0";
            $resultado=$conexion->prepare($sql);
            $resultado->execute();
            $res=$resultado->fetch();

            /**
             * Si el usaurio esta activo devuelve status_code=0 sino false
             */
            if(!empty($res)){
                error_log('Procesamos request activateUser | username: '.$request['username']);
                return array(
                    "Resultado" =>'status_code =0'
                );
            }else{
                error_log('Procesamos request activateUser | username: '.$request['username']);
                return array(
                    "Resultado" => false
                );
            }

        }else{
            error_log('Hubo un error al intentar procesar request activateUser');
        }

}



/**Agregar Usuario
 * @param $request
 * @return false[]|string[]
 */
function addUser($request){
  $conexion=  Conexion::conectarse();

  $sql="insert into users (username,password,email) 
values(:username,:password,:email)";

    $resultado = $conexion->prepare($sql);

        if(isset($request['username']) && isset($request['password']) && isset($request['email'])){

            $resultado->bindParam(':username',$request['username'],PDO::PARAM_STR, 25);
            $resultado->bindParam(':password',$request['password'],PDO::PARAM_STR, 25);
            $resultado->bindParam(':email',$request['email'],PDO::PARAM_STR,25);
            $resultado->execute();

            $lastInsertId = $conexion->lastInsertId();
            if($lastInsertId>0){
                error_log('Procesamos request addUser | username: '.$request['username'].'| password:'.$request['password'].'| email:'.$request['email']);
                return array(
                    "Resultado" =>'status_code =0'
                );
            }else{
                error_log('Hubo un error al intentar procesar request addUser');
                return array(
                    "Resultado" => false
                );
            }
        }else{
            error_log('Hubo un error al intentar procesar request addUser');
        }

}

$POST_DATA = file_get_contents("php://input");
$server->service($POST_DATA);
exit();
